import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as synced_folder from "@pulumi/synced-folder";

const config = new pulumi.Config();
const folderPublico = config.require("folderPublico");
const indexPage = "index.html";
const errorPage = "404.html";
const siteBucket = "bucketWebSite"
const targetDomain = "www.dennisaviles.net.pe"

const bucket = new gcp.storage.Bucket(siteBucket, {
    location: "US",
    autoclass: {
        enabled: false
    },
    storageClass: "STANDARD",
    website: {
        mainPageSuffix: indexPage,
        notFoundPage: errorPage
    },
    name: targetDomain
});

const bucketIamBinding = new gcp.storage.BucketIAMBinding("binding", {
    bucket: bucket.name,
    role: "roles/storage.objectViewer",
    members: ["allUsers"]
});

const syncFolder = new synced_folder.GoogleCloudFolder("syncFolder", {
    path: folderPublico,
    bucketName: bucket.name
})